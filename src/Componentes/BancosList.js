import React, { Fragment, useEffect, useState } from 'react'
import '../Assets/stylos/styled.scss'

function BancosList() {
    //Declaracion de direccción de la apii
    const url = 'https://api.jsonbin.io/b/604006e581087a6a8b95b784';


    //Declaracion del hook estado inicial de bancos
    const [bancos, todosBancos] = useState()

    const fetchApi = async () => {
        const response = await fetch(url)

        const responseJson = await response.json();
        console.log(responseJson)
        todosBancos(responseJson)

    }

    useEffect(() => {
        fetchApi();
    }, [])

    return (

        <Fragment>

            <div className="row no-gutters">
                <div className="container">
                    <div className="table-responsive">
                        <table class="table">
                            <thead class="thead-dark">
                                <tr>
                                    <th scope="col">Banco</th>
                                    <th scope="col">Descripcion</th>
                                    <th scope="col">Edad</th>
                                    <th scope="col">URL</th>

                                </tr>
                            </thead>
                            <tbody>
                                {!bancos ? 'Cargando...' :
                                    bancos.map((todo, index) => {
                                        return <tr>
                                            <td>{todo.bankName}</td>
                                            <td>{todo.description}</td>
                                            <td>{todo.age}</td>
                                            <td><a href={todo.url} target="_blank" className="url"> {todo.url}</a> </td>

                                        </tr>
                                    })}
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>


        </Fragment>
    )
}
export default BancosList;