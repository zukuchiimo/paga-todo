import React from 'react'
import logo from '../Assets/imagenes/logo.jpg'
import '../Assets/stylos/styled.scss'

function Menu() {
    return (
        <nav className="navbar navbar-expand-lg navbar-color ">
            <div className="container-fluid">
                <a className="navbar-brand" href="#">
                    <img src={logo}  className="logo"/>
                    
                </a>
                <button className="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                    <span className="navbar-toggler-icon">
                        
                    </span>
                </button>
                
             </div>
         </nav>
                )
}
export default  Menu; 