import React, { Fragment } from 'react'
import car1 from '../Assets/imagenes/empresario.jpg'
import car2 from '../Assets/imagenes/exito.jpg'
import car3 from '../Assets/imagenes/tienda.jpg'
import '../Assets/stylos/styled.scss'

function Carrusel() {
    return (

        <Fragment>
            <div id="carouselExampleControls" class="carousel slide" data-ride="carousel">
                <div class="carousel-inner">
                    <div class="carousel-item active">
                        <img class="d-block  carrusel img-fluid" src={car1} alt="First slide" />
                    </div>
                    <div class="carousel-item">
                        <img class="d-block carrusel img-fluid" src={car2} alt="Second slide " />
                    </div>
                    <div class="carousel-item">
                        <img class="d-block carrusel img-fluid" src={car3} alt="Third slide" />
                    </div>
                </div>
                <a class="carousel-control-prev" href="#carouselExampleControls" role="button" data-slide="prev">
                    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                    <span class="sr-only">Previous</span>
                </a>
                <a class="carousel-control-next" href="#carouselExampleControls" role="button" data-slide="next">
                    <span class="carousel-control-next-icon" aria-hidden="true"></span>
                    <span class="sr-only">Next</span>
                </a>
            </div>
            <div className="row no-gutters">
            <h1 className="mt-4 mx-auto">Lista de Bancos</h1>
            </div>

        </Fragment>
    )
}
export default Carrusel;