import Menu from './Componentes/Menu';
import Carrusel from './Componentes/Carrusel';
import BancosList from './Componentes/BancosList';
import Footer from './Componentes/Footer';

import './App.css';

function App() {
  return (
<div>
  <Menu/>
  <Carrusel/>
  <BancosList/>
  <Footer/>
</div>
  );
}

export default App;
